const coin = {
    state: 0,
    flip: function() {
        // 1. One point: Randomly set your coin object's "state" property to be either 
        //    0 or 1: use "this.state" to access the "state" property on this object.
        this.state = Math.round(Math.random())
    },
    toString: function() {
        // 2. One point: Return the string "Heads" or "Tails", depending on whether
        //    "this.state" is 0 or 1.
        return this.state ? "Heads" : "Tails"
    },
    toHTML: function() {
        const image = document.createElement('img');
        // 3. One point: Set the properties of this image element to show either face-up
        //    or face-down, depending on whether this.state is 0 or 1.
        image.src = this.state ? './images/heads.png' : './images/tails.png';
        return image;
    }
};
function display20Flips() {
    // 4. One point: Use a loop to flip the coin 20 times, each time displaying the result of the flip as a string on the page.  After your loop completes, return an array with the result of each flip.
    let flips = [];
    for(let i = 0; i < 20; i++) {
        coin.flip();
        let flip = coin.toString();
        let result = document.createElement('div');
        result.textContent = flip;
        flips.push(flip);
        document.body.appendChild(result);
        
    }

    return flips;
}
function display20Images() {
    // 5. One point: Use a loop to flip the coin 20 times, and display the results of each flip as an image on the page.  After your loop completes, return an array with result of each flip.
    let flips = [];
    for(let i = 0; i < 20; i++) {
        coin.flip();
        let flip = coin.toString();
        flips.push(flip);
        document.body.appendChild(coin.toHTML())
    }

    return flips;
}

display20Flips();
display20Images();